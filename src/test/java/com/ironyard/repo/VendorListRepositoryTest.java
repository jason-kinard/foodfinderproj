package com.ironyard.repo;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ironyard.springboot.data.Vendor;
import com.ironyard.springboot.data.VendorType;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class VendorListRepositoryTest {
	
	@Autowired
	private VendorRepository theRepo;
	
	@Before
	public void setup(){

	}
	
	
	@Test
	public void testCreation() throws Exception {
		Vendor 	newVendor = new Vendor();
		newVendor.setName("Test Vendor 1");
		newVendor.setDescription("Test Description 1");
		newVendor.setType(VendorType.BURGERS);
		newVendor.setRating(4);
		newVendor.setCostPerPerson(2.55f);
		
		
		//Save the list to the repo
		theRepo.save(newVendor);
		
		//Test for the newly created object
		Assert.assertTrue(theRepo.findByType(VendorType.BURGERS).size() == 1);
		Assert.assertTrue(theRepo.findByName("Test Vendor 1").size() == 1);
	}
	@Test
	public void testUpdate() throws Exception {
		//Create new vendor
		Vendor 	newVendor = new Vendor();
		newVendor.setName("Test Vendor Update");
		newVendor.setDescription("Test Description 2");
		newVendor.setType(VendorType.ITALIAN);
		newVendor.setRating(5);
		newVendor.setCostPerPerson(5.55f);
		
		//add new vendor
		theRepo.save(newVendor);
		
		List<Vendor> returnList = new ArrayList<Vendor>();
		
		returnList = theRepo.findByType(VendorType.ITALIAN);
		
		Assert.assertNotNull(returnList.get(0));
		long existingId = returnList.get(0).getId();
		Assert.assertTrue(returnList.get(0).getName().equals("Test Vendor Update"));
		
		//update vendor
		newVendor.setId(existingId);
		newVendor.setName("Test Vendor Name Updated");
		theRepo.save(newVendor);
		
		Assert.assertTrue(theRepo.findOne(existingId).getName().equals("Test Vendor Name Updated"));
	}
	@Test
	public void testDeletion() throws Exception {
		Vendor newVendor = new Vendor();
		List<Vendor> vendorList = new ArrayList<Vendor>();
		newVendor.setName("Test Vendor 2");
		newVendor.setDescription("Test Description 2");
		newVendor.setType(VendorType.ITALIAN);
		newVendor.setRating(5);
		newVendor.setCostPerPerson(50.50f);
		vendorList.add(newVendor);
		
		theRepo.deleteAll();
		
		Assert.assertTrue(theRepo.count() == 0);
	}

	@Test
	public void testFindByName() throws Exception {
		Vendor 	newVendor = new Vendor();
		newVendor.setName("Test Vendor Find By Name");
		newVendor.setDescription("Test Find By Name Description");
		newVendor.setType(VendorType.ICE_CREAM);
		newVendor.setRating(1);
		newVendor.setCostPerPerson(1.55f);
		
		
		//Save the list to the repo
		theRepo.save(newVendor);
		
		List<Vendor> vendorList = new ArrayList<Vendor>();
		
		vendorList = theRepo.findByName("Test Vendor Find By Name");

		Assert.assertNotNull(vendorList.get(0));
		Assert.assertTrue(vendorList.get(0).getName().equals("Test Vendor Find By Name"));
		Assert.assertTrue(vendorList.get(0).getDescription().equals("Test Find By Name Description"));
		
	}
	@Test
	public void testGetInvalidID() throws Exception {
		Vendor foundVendor = theRepo.findOne((long) 99999999);
		
		Assert.assertNull(foundVendor);
	}
}
